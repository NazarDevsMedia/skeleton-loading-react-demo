import React, { Component } from "react";
import LoadSkeleton from "./components/LoadSkeleton";

class App extends Component {
  render() {
    return (
      <>
        <LoadSkeleton />
      </>
    );
  }
}

export default App;
