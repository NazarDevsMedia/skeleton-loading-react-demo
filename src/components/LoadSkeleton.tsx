import React, { Component } from 'react'

import Skeleton from 'react-loading-skeleton';
 
export default class LoadSkeleton extends Component {
    state = {
        title: "",
        body: ""
    }
    componentDidMount(){
        setTimeout(() => {
            this.setState({title: "Test Title", body: "test body test body test body"})
        }, 5000);
    }
    render() {
      return (
        <div style={{ fontSize: 20, lineHeight: 2, width: "50%", margin: "0 auto" }}>
          <h1>{this.state.title || <Skeleton width={400} />}</h1>
          {this.state.body || <Skeleton count={2} />}
        </div>
      );
    }
  }