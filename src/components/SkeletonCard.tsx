import * as React from "react";
import {
  createSkeletonProvider,
  createSkeletonElement
} from "@trainline/react-skeletor";
import "./CardCSS.css";
import { Card } from "./Data";
import {Icon} from "./Icon";

const dummyData = {
  card: {
    title: "______________ _________",
    description: `_______`,
    avatar: ""
  }
};

export interface Props {
  card?: Card;
}

const Span = createSkeletonElement("span");
const Img = createSkeletonElement("img");
const Pt = createSkeletonElement("p");
const IconP = createSkeletonElement(Icon);

export const CardComponent: React.StatelessComponent<Props> = ({ card }) => (
  <div>
    <div className="card__container">
      <IconP />
      <div className="card__content">
        <h1 className="card__first-name">
          <Span>{card!.title}</Span>
        </h1>
        <Pt className="card__description">{card!.description}</Pt>
        {/* <Div className="card__description">{card!.description}</Div> */}
      </div>
    </div>
  </div>
);

export default createSkeletonProvider(
  dummyData,
  // Declare pending state if data is undefined
  ({ card }) => card === undefined,
  // Pass down pending className, defined in index.ejs of this project
  "pending"
)(CardComponent);
