export interface Card {
  title: string;
  description: string;
  avatar: string;
}

export interface ApplicationState {
  cardA?: Card;
}

// Fake API
const data: ApplicationState = {
  cardA: {
    title: "Aspirin Heißgetränk",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consectetur metus in nibh porttitor ultricies. Vestibulum placerat blandit interdum.", //tslint:disable-line
    avatar:
      "https://cdn-images-1.medium.com/max/1200/1*nQOh17AII63UbEakSAWICQ.png"
  }
};

export default data;
