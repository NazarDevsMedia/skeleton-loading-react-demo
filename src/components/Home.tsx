import * as React from "react";
import styled from "styled-components";
import CardCSS from "./SkeletonCard";
import data, { ApplicationState } from "./Data";
import {Icon} from "./Icon";

const fakeAPI = () =>
  new Promise<ApplicationState>((resolve, reject) =>
    setTimeout(() => resolve(data), 2000)
  );

const Container = styled.div`
  margin-top: 40px;
  text-align: left;
  width: 70%;
  margin: auto;
`;

const Button = styled.button`
  border: none;
  background-color: #26ee29;
  border-radius: 5px;
  padding: 6px 20px;
  color: white;
  margin-bottom: 20px;
  margin-top: 40px;
`;

const initialState: ApplicationState = {
  cardA: undefined
};

export default class Home extends React.Component<{}, ApplicationState> {
  state = initialState;

  componentWillMount() {
    fakeAPI().then(response => this.setState(response));
  }

  onLoadData = () => {
    if (this.state.cardA) {
      this.setState(initialState);
      fakeAPI().then(response => this.setState(response));
    }
  };

  render() {
    return (
      <>
      <Container>
        <Button onClick={this.onLoadData}>Fetch data</Button>
        <CardCSS card={this.state.cardA} />
      </Container>
        <Icon />
        </>
    );
  }
}

